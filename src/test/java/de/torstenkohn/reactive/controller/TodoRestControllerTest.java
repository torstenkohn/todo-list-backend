package de.torstenkohn.reactive.controller;

import de.torstenkohn.reactive.domain.Todo;
import de.torstenkohn.reactive.repository.TodoRepository;
import de.torstenkohn.reactive.service.TodoService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class TodoRestControllerTest {


    @Autowired
    private TodoRepository repository;

    @Autowired
    private TodoService service;

    private WebTestClient client;

    private static final Todo[] TEST_TODOS = {
            new Todo("t_1", "Wohnung putzen", true),
            new Todo("t_2", "Todo ausdenken", false)
    };

    @Before
    public void setUp() {
        client = WebTestClient.bindToController(new TodoRestController(service)).build();
        this.repository
                .deleteAll()
                .thenMany(Flux.fromArray(TEST_TODOS)
                        .flatMap(this.repository::save))
                .subscribe();
    }

    @Test
    public void testAllTodos() {
        this.client.get().uri("/api/todos")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Todo.class).hasSize(2)
                .contains(TEST_TODOS);
    }

    @Test
    public void testForValidId() {
        this.client.get().uri("/api/todos/t_1")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.title", "Wohnung putzen").exists()
                .jsonPath("$.completed", "true").exists();
    }

    @Test
    public void testForUnavailableId() {
        this.client.get().uri("/api/todos/unavailable")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody().isEmpty();
    }

    /*
    The next both test are only for showing the checkpoint method which enables assembly tracing
     */
    @Test
    @Ignore
    public void testNullPointer() {
        Flux.just(
                new Todo("t_1", "Wohnung putzen", true),
                null,
                new Todo("t_2", "Todo ausdenken", false))
                .filter(Todo::isCompleted)
                .subscribe(System.out::println);
    }

    @Test
    @Ignore
    public void testNullPointerWithCheckpoint() {
        Flux.just(
                new Todo("t_1", "Wohnung putzen", true),
                null,
                new Todo("t_2", "Todo ausdenken", false))
                .filter(Todo::isCompleted)
                .checkpoint("Todo - Checkpoint", true)
                .subscribe(System.out::println);
    }
}