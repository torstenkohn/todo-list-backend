package de.torstenkohn.reactive;

import de.torstenkohn.reactive.domain.Todo;
import de.torstenkohn.reactive.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class TodoListApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoListApplication.class, args);
    }

    /**
     * The runner will initial add items to the database during the startup of the application.
     * It is only used for the demo
     */
    @Component
    class UserInitCommandLineRunner implements CommandLineRunner {

        private final TodoRepository repository;

        @Autowired
        UserInitCommandLineRunner(TodoRepository repository) {
            this.repository = repository;
        }

        @Override
        public void run(String... args) {
            this.repository
                    .deleteAll()
                    .thenMany(Flux
                            .just(
                                    "Wohnung putzen",
                                    "Effective Java kaufen",
                                    "Reactive Spring lernen",
                                    "Milch einkaufen",
                                    "Java 9 installieren",
                                    "Buch Lesen: How to un-fuck git",
                                    "Todo ausdenken")
                            .map(Todo::new)
                            .flatMap(this.repository::save))
                    .subscribe(null, null,
                            () -> this.repository
                                    .findAll()
                                    .subscribe(System.out::println));
        }
    }
}
