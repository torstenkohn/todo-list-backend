package de.torstenkohn.reactive.controller;

import de.torstenkohn.reactive.domain.Todo;
import de.torstenkohn.reactive.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/todos")
public class TodoRestController {

    private final TodoService service;

    @Autowired
    public TodoRestController(TodoService service) {
        this.service = service;
    }

    @GetMapping
    public Flux<Todo> all() {
        return this.service.all();
    }

    // this is the original Code from the article.
    // Below you can find a improved version which will response with 404 if the ID does not exists.
//    @GetMapping("/{todoId}")
//    public Mono<Todo> byId(@PathVariable String todoId) {
//        return this.service.byId(todoId);
//    }

    /**
     * Request a item for the ID
     *
     * @param todoId the id for the requested item
     * @return status code 200 with the requested item or status code 404 if the requested item does not exists
     */
    @GetMapping("/{todoId}")
    public Mono<ResponseEntity<Todo>> byId(@PathVariable String todoId) {
        return this.service.byId(todoId)
                .map(todo -> ok()
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .body(todo))
                .defaultIfEmpty(notFound().build());
    }

    @PostMapping
    public Mono<Todo> add(@RequestBody Todo todo) {
        return this.service.save(todo);
    }

    @PutMapping("/{todoId}")
    public Mono<Todo> update(@PathVariable String todoId, @RequestBody Todo todo) {
        return this.service.update(todoId, todo);
    }

    @DeleteMapping("/{todoId}")
    public Mono<Void> deleteById(@PathVariable String todoId) {
        return this.service.deleteById(todoId);
    }

}
