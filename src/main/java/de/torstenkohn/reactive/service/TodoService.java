package de.torstenkohn.reactive.service;

import de.torstenkohn.reactive.domain.Todo;
import de.torstenkohn.reactive.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TodoService {
    private final TodoRepository repository;

    @Autowired
    public TodoService(TodoRepository repository) {
        this.repository = repository;
    }

    public Flux<Todo> all() {
        return repository.findAll();
    }

    public Mono<Todo> byId(final String todoId) {
        return repository.findById(todoId);
    }

    public Mono<Todo> save(Todo todo) {
        return this.repository.save(todo);
    }

    public Mono<Todo> update(String todoId, Todo todo) {
       return byId(todoId)
                .map(repositoryItem -> {
                    repositoryItem.setCompleted(todo.isCompleted());
                    repositoryItem.setTitle(todo.getTitle());
                    return repositoryItem;
                })
               .flatMap(this::save);
    }

    public Mono<Void> deleteById(String todoId) {
        return this.repository.deleteById(todoId);
    }
}
