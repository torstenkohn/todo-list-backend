package de.torstenkohn.reactive.repository;

import de.torstenkohn.reactive.domain.Todo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface TodoRepository extends ReactiveMongoRepository<Todo, String> {

}
